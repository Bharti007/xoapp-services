import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import {TabsPage} from "../tabs/tabs";
import {RegisterPage} from "../register/register";
import {LoginProvider} from "../../providers/login";
import {Validator} from "../../validator"
import { FormBuilder, FormGroup } from '@angular/forms';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
    private loginForm : FormGroup;
  constructor(public navCtrl: NavController,public alertCtrl:AlertController, public navParams: NavParams,public loginPr:LoginProvider,private formBuilder: FormBuilder) {
      this.loginForm = this.formBuilder.group({
          email: Validator.emailValidator,
          password: Validator.passwordValidator

      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');

  }

  login(){
      this.loginPr.emailLogin(this.loginForm.value.email,this.loginForm.value.password)
  }
  register(){
    this.navCtrl.setRoot(RegisterPage)
  }

    //forgot password
    resetPassword(){
        let alert = this.alertCtrl.create();
        alert.setTitle('Forget password');

        alert.addInput({
            type: 'text',
            placeholder: 'Please Enter Your Email',
        });
        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: data => {
                let passData = data[0];
                this.changepass(passData);
            }
        });
        alert.present();

    }

    pswreset(){
        let psw = this.alertCtrl.create({
            title: 'Alert',
            subTitle: 'password reset',
            buttons: ['OK']
        });
        psw.present();
    }

    changepass(data){
        let email= data;
        // this.usersService.passReset(email).then(() => alert("password reset"))
        this.loginPr.sendPasswordReset(email).then(() => this.pswreset())
            .catch((_error) => alert(_error));
    }

}
