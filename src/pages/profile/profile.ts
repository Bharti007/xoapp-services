import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ActionSheetController } from 'ionic-angular';
import {Validator} from "../../validator";
import {FormBuilder, FormGroup} from "@angular/forms";
import {CrudProvider} from "../../providers/crud";

//firebase
import * as firebase from 'firebase';
import 'firebase/firestore';
import {Camera, CameraOptions} from "@ionic-native/camera";
/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
    selector: 'page-profile',
    templateUrl: 'profile.html',
})
export class ProfilePage {
    private profileForm : FormGroup;
    public curUser:any;
    public profileImage:any;
    private db:any;
    public users=[];
    constructor(private camera: Camera,public navCtrl: NavController, public navParams: NavParams,private formBuilder: FormBuilder,
                public actionSheetCtrl: ActionSheetController
        ,public crud: CrudProvider) {

        this.db = firebase.firestore();
        this.profileForm = this.formBuilder.group({
            username: Validator.usernameValidator,
            email: Validator.emailValidator,
            phone: Validator.phoneValidator,

        });

        this.crud.getCurrentUser().then((user)=>{
            if(user != null){
                this.crud.getSingleDocument('users',user.uid).then((result)=>{
                    if(result != null){
                        console.log(result);
                        this.curUser = result;
                        this.profileImage = result.image;
                        this.profileForm.get('email').setValue(result.email);
                        this.profileForm.get('username').setValue(result.username);
                        this.profileForm.get('phone').setValue(result.phone);
                    }
                    else{
                        this.navCtrl.pop();
                    }
                })
            }
        });
    }

    ionViewDidLoad() {
        this.saveDatabase("");
        console.log('ionViewDidLoad ProfilePage');
    }

    profileUpdate() {
        var data;
        console.log( this.profileForm.value);
        this.crud.updateDocument('users', this.curUser.userid, this.profileForm.value).then((dataUpdated) => {
            console.log(dataUpdated);
        })

    }

    captureDataUrl:any;
//open camera picture
    openCamera() {
        const cameraOptions: CameraOptions = {
            targetHeight:150,
            targetWidth:150,
            allowEdit:true,
            quality: 50,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
        };
        this.camera.getPicture(cameraOptions).then((imageData) => {
            this.captureDataUrl = 'data:image/jpeg;base64,' + imageData;
            console.log("CaptureDataUrl:" + this.captureDataUrl);
            this.upload();
        }, (err) => {
        });
    }
//open galary picture
    openGalary(){
        const cameraOptions: CameraOptions = {
            targetHeight:150,
            targetWidth:150,
            allowEdit:true,
            quality: 50,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        };
        this.camera.getPicture(cameraOptions).then((imageData) => {
            this.captureDataUrl = 'data:image/jpeg;base64,' + imageData;
            console.log("CaptureDataUrl:" + this.captureDataUrl);
            this.upload();
        }, (err) => {
        });
    }
//upload firebase
    upload() {
        let storageRef = firebase.storage().ref();
        const filename = Math.floor(Date.now() / 1000);
        const imageRef = storageRef.child(`images/${filename}.jpg`);
        console.log("Upload Const ImageRef:" + imageRef);
        imageRef.putString(this.captureDataUrl, firebase.storage.StringFormat.DATA_URL).then((snapshot)=> {
            console.log("SnapShot:" + JSON.stringify(snapshot.downloadURL));
            this.saveDatabase(snapshot);
        });
    }
//upload database
    saveDatabase(snapshot){
        // let imgUrl=snapshot.downloadURL;
        // console.log(this.curUser);
        //
        // this.db.collection('users').doc(this.curUser.userid).update({
        //     "image":snapshot.downloadURL
        // }).then((data)=>{
        //     console.log(data);
        // });

        let user = firebase.auth().currentUser;
         console.log(user);
        var ref= this.db.collection('users').doc(user.uid).collection("booking");
        ref.onSnapshot((snap1)=> {
            snap1.forEach((doc,key) => {
                var obj = JSON.parse(JSON.stringify(doc.data()));
                console.log(obj);
                obj.uid = obj.jobId;
                if(obj.jobId != user.uid){
                    this.users.push(obj);
                }

            });
            this.bookingsUpdateData(this.users, "/assets");
        });

    }

    bookingsUpdateData(userIds,imgUrl){
        let currentuser = firebase.auth().currentUser;
        var ref = this.db.collection('users').doc(currentuser.uid);
        ref.onSnapshot((snap2)=>{
            let userType=snap2.data().type;
            if(userType=="Customer"){
                for(let i=0; i<userIds.length; i++){
                    console.log(userIds[i].uid);
                    var bookingref = this.db.collection('bookings').doc( userIds[i].uid);
                    console.log(userIds);
                     let userBookings = this.db.collection('/users').doc(userIds[i].serviceProvId).collection( 'booking').doc( userIds[i].uid);
                    bookingref.update({
                            "customerImage":imgUrl
                        }).then((data)=>{
                            console.log(data);
                        });

                    userBookings.update({
                        "customerImage":imgUrl
                    }).then((data)=>{
                        console.log(data);
                    });

                }
            }else{
                for(let i=0; i<userIds.length; i++){
                    console.log(userIds[i].uid);
                    var bookingref = this.db.collection('bookings').doc( userIds[i].uid);

                    console.log(bookingref);
                   let userBookings = this.db.collection('/users').doc(userIds[i].customerId).collection( 'booking').doc( userIds[i].uid);
                    console.log(userBookings);
                   bookingref.update({
                        "serviceProvimage":imgUrl
                    }).then((data)=>{
                        console.log(data);
                    });

                    userBookings.update({
                        "serviceProvimage":imgUrl
                    }).then((data)=>{
                        console.log(data);
                    });

                }
                // for(let i=0; i<userIds.length; i++){
                //     let id = userIds[i].uid;
                //     var bookingref = firebase.database().ref('/bookings/' + userIds[i].uid);
                //     let userBookings = firebase.database().ref('/users/'+ userIds[i].customer + '/bookings/' + userIds[i].uid);
                //     bookingref.child("serviceProvimage").set(imgUrl);
                //     userBookings.child("serviceProvimage").set(imgUrl);
                // }
            }
        })
    }

//option Select Camera or Album
    doGetPicture(){
        let actionSheet = this.actionSheetCtrl.create({
            buttons: [
                {
                    text: 'Camera',
                    role: 'camera',
                    handler: () => {
                        this.openCamera();
                    }
                },{
                    text: 'Album',
                    handler: () => {
                        this.openGalary();
                    }
                },{
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    }
//End Camera and Album related work

}
