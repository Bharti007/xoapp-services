import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import * as firebase from 'firebase';
import 'firebase/firestore';
import {ServerService} from "../../providers/server-service";
import {LoadingProvider} from "../../providers/loading";
import {TabsPage} from "../tabs/tabs";
import {CallNumber} from '@ionic-native/call-number';
import {MessagesPage} from "../messages/messages";

/**
 * Generated class for the CurrentJobsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-current-jobs',
  templateUrl: 'current-jobs.html',
})
export class CurrentJobsPage {
    public job:any;
    private db: any;
    private stripeID:any;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public serverServ:ServerService,
              public loader:LoadingProvider,
              public alertCtrl:AlertController,
              public callNo:CallNumber) {
      this.job = this.navParams.data.job;
      this.db = firebase.firestore();
      console.log(this.job)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CurrentJobsPage');
  }

    completeJob(job){
      console.log(job);

       this.loader.show();
       var booking = this.db.collection('bookings').doc(job.jobId);
      var customer = this.db.collection('users').doc(this.job.customerId).collection('booking').doc(job.jobId);
      var serviceProvider = this.db.collection('users').doc(job.serviceProvId).collection('booking').doc(job.jobId);

        let totalBill = job.serviceCost;

        var paystripe = this.db.collection('users').doc(this.job.customerId);
        paystripe.onSnapshot( (_snapshot:any) => {
            console.log(_snapshot.data());
            if(_snapshot.data()){
                this.stripeID=_snapshot.data().stripeID;
                let cards = _snapshot.data().payments;
                let GotIt =false;
                let cardID = "";
                for(let i=0;i<cards.length;i++){
                    if(cards[i].primary){
                        GotIt = true;
                        cardID = cards[i].stripeCardID;
                    }

                    if(GotIt){
                        console.log(cardID,totalBill)
                        this.serverServ.stripePayment(cardID,this.stripeID,totalBill).subscribe(response=>{
                            console.log("stripe payment response",response);
                            let temp :any;
                            temp = response;
                            if(temp.status =="succeeded"){
                                let alert = this.alertCtrl.create();

                                alert.setTitle('Alert');
                                alert.setMessage('Payment successfully deducted');
                                alert.addButton({
                                    text: 'Dismiss',
                                    handler: data => {
                                        this.navCtrl.setRoot(TabsPage);
                                        alert.dismiss();
                                    }
                                });
                                alert.present();

                                this.serverServ.sendPush('Service completed',"Your service cost is $" + totalBill ,paystripe).subscribe(response=>console.log(response));
                                 booking.update({'status': 'Review'});
                                 customer.update({'status': 'Review'});
                                 serviceProvider.update({'status': 'Review'});
                                this.loader.hide();
                                this.navCtrl.pop();

                            }else{
                                alert(temp.message);
                                this.serverServ.sendPush('Payment Not Approve !!',"No Payment Method Found. Your service cost is $" + totalBill ,paystripe).subscribe(response=>console.log(response));
                                this.loader.hide();
                                this.navCtrl.pop();
                            }
                        },err=>{
                            alert('Server Error ! Try again');
                            booking.update({'status': 'Review'});
                            customer.update({'status': 'Review'});
                            serviceProvider.update({'status': 'Review'});
                            this.loader.hide();
                        });
                    }else{
                        alert("Customer Not Selected Primary card");
                        this.loader.hide();
                        this.navCtrl.pop();

                        this.serverServ.sendPush('Payment Failed !!',"Not Selected Primary card. Your service cost is $" + totalBill ,paystripe).subscribe(response=>console.log(response));
                    }
                }
            }

            else{
                alert("Customer Card Not Available");
                this.loader.hide();
                this.navCtrl.pop();
                this.serverServ.sendPush('Payment Failed !!'," Card Not Available. Your cost for the booking is $" + totalBill,paystripe).subscribe(response=>console.log(response));
            }
        });




  }

  cancelJob(job){
      this.loader.show();
      var booking = this.db.collection('bookings').doc(job.jobId);
      var customer = this.db.collection('users').doc(this.job.customerId).collection('booking').doc(job.jobId);
      var serviceProvider = this.db.collection('users').doc(job.serviceProvId).collection('booking').doc(job.jobId);
      booking.update({'status': 'Cancelled'});
      customer.update({'status': 'Cancelled'});
      serviceProvider.update({'status': 'Cancelled'});
      // this.serverServ.sendPush('Job completed ',  this.job.customerName + "accepted your bid.", bidding.serviceProvpushToken).subscribe(response => console.log(response));
      this.loader.hide();
      this.navCtrl.pop();

  }

    callUser() {
        let customerRef = this.db.collection('users').doc(this.job.customerId);

        customerRef.onSnapshot((snapshot: any) => {
            if (snapshot.data() && snapshot.data() != null && snapshot.data().hasOwnProperty('phone')) {
                var callCustomer;
                callCustomer = snapshot.data().phone;

                    this.callNo.callNumber(callCustomer, true)
                        .then(() => console.log('Launched dialer!'))
                        .catch(() => console.log('Error launching dialer'));

            }
            else {

                let alert = this.alertCtrl.create();

                alert.setTitle('Alert');
                alert.setMessage('No phone number is provided by the customer');
                alert.addButton({
                    text: 'Dismiss',
                    handler: data => {
                        alert.dismiss();
                    }
                });
                alert.present();
            }
        });
    }

    negotiateJob(job){
        this.navCtrl.push(MessagesPage,{"job":job});
    }

}
