import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import {PendingJobsPage} from "../pending-jobs/pending-jobs";
import {PastJobsPage} from "../past-jobs/past-jobs";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = PendingJobsPage;
  tab3Root = PastJobsPage;

  constructor() {

  }
}
