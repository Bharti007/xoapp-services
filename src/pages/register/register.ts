import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {TabsPage} from "../tabs/tabs";
import {LoginPage} from "../login/login";
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import {AbstractControl} from '@angular/forms';
import {Validator} from "../../validator"
import {LoginProvider} from "../../providers/login"

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
    private registerForm : FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams,public registerPro:LoginProvider,private formBuilder: FormBuilder) {
     // let EMAILPATTERN = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
      this.registerForm = this.formBuilder.group({
          username: Validator.usernameValidator,
          email: Validator.emailValidator,
          password: Validator.passwordValidator,
          confirmpassword:Validator.passwordValidator,
          phone: Validator.phoneValidator,

      },{
          validator: this.MatchPassword // your validation method
      });
  }

    public MatchPassword(AC: AbstractControl) {
        let password = AC.get('password').value; // to get value in input tag
        let confirmPassword = AC.get('confirmpassword').value; // to get value in input tag
        if(password != confirmPassword) {
            AC.get('confirmpassword').setErrors( {MatchPassword: true} )
        } else {
            return null
        }
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  login(){
    this.navCtrl.setRoot(LoginPage)
  }
  register(registerData){
    this.registerPro.register(this.registerForm.value)
  }

}
