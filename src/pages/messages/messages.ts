import { Component,ViewChild } from '@angular/core';
import { NavController, NavParams ,Content} from 'ionic-angular';
import * as firebase from 'firebase';
import 'firebase/firestore';
import {ServerService} from "../../providers/server-service";
import {LoadingProvider} from "../../providers/loading";
import {CrudProvider} from "../../providers/crud";


/**
 * Generated class for the MessagesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-messages',
  templateUrl: 'messages.html',
})
export class MessagesPage {
    @ViewChild(Content) content: Content;
    private db: any;
    public curUser:any='';
    public job:any;
    private messagesToShow: any;
    private message: any;
    private conversationId: any;
    private messages: any;
    private startIndex: any = -1;
    // Set number of messages to show.
    private numberOfMessages = 10;
    private updateDateTime: any;

  constructor(public crud: CrudProvider,public navCtrl: NavController, public navParams: NavParams,public serverServ:ServerService,public loader:LoadingProvider) {
      this.db = firebase.firestore();
      this.job = this.navParams.data.job;
      console.log(this.job);

      //to get the current user
      this.crud.getCurrentUser().then((user)=>{
          console.log(user)
          this.getMessages(user.uid);
          this.scrollBottom(user.uid);
          if(user != null){
              this.crud.getSingleDocument('users',user.uid).then((result)=>{
                  console.log(result);
                  if(result != null){
                      this.curUser = result;

                  }
                  else{
                      this.navCtrl.pop();
                  }
              })
          }
      });
  console.log(this.curUser);


  }

    ionViewDidEnter(){

    }
    // Load previous messages in relation to numberOfMessages.
    loadPreviousMessages() {
        var that = this;
        // Show loading.
        this.loader.show();
        setTimeout(function() {
            // Set startIndex to load more messages.
            if ((that.startIndex - that.numberOfMessages) > -1) {
                that.startIndex -= that.numberOfMessages;
            } else {
                that.startIndex = 0;
            }
            // Refresh our messages list.
            that.messages = null;
            that.messagesToShow = null;

            that.scrollTop();

            // Populate list again.
            that.getMessages(this.curUser.userid);
        }, 1000);
    }

    ionViewWillLeave() {
        this.setMessagesRead(this.curUser.userid);
    }


  getMessages(curUserId){
      //to get the messages history if any
      this.db.collection('users').doc(curUserId).collection('conversations').doc(this.job.customerId)
          .onSnapshot((querySnapshot1) => {
              console.log(querySnapshot1.data());
              if(querySnapshot1.data() != undefined){

                  this.conversationId = querySnapshot1.data().conversationId;
                  this.db.collection('conversations').doc(this.conversationId).onSnapshot((querySnapshot) => {
                      console.log(querySnapshot.data());
                      //querySnapshot.forEach((doc) => {
                      // var obj = JSON.parse(JSON.stringify(doc.data()));
                      let messages = querySnapshot.data().messages;
                      if (this.messages) {
                          // Just append newly added messages to the bottom of the view.
                          if (messages.length > this.messages.length) {
                              let message = messages[messages.length - 1];
                              // this.dataProvider.getUser(message.sender).snapshotChanges().subscribe((user) => {
                              //     message.avatar = user.payload.val().img;
                              // });
                              this.messages.push(message);
                              this.messagesToShow.push(message);
                          }
                      }
                      else {
                          // Get all messages, this will be used as reference object for messagesToShow.
                          this.messages = [];
                          messages.forEach((message) => {
                              // this.dataProvider.getUser(message.sender).snapshotChanges().subscribe((user) => {
                              //     message.avatar = user.payload.val().img;
                              // });
                              this.messages.push(message);
                          });
                          // Load messages in relation to numOfMessages.
                          if (this.startIndex == -1) {
                              // Get initial index for numberOfMessages to show.
                              if ((this.messages.length - this.numberOfMessages) > 0) {
                                  this.startIndex = this.messages.length - this.numberOfMessages;
                              } else {
                                  this.startIndex = 0;
                              }
                          }
                          if (!this.messagesToShow) {
                              this.messagesToShow = [];
                          }
                          // Set messagesToShow
                          for (var i = this.startIndex; i < this.messages.length; i++) {
                              this.messagesToShow.push(this.messages[i]);
                          }
                          this.loader.hide();
                      }
                      // });



                  });

              }



          });


      var that = this;
      if (!that.updateDateTime) {
          that.updateDateTime = setInterval(function() {
              if (that.messages) {
                  that.messages.forEach((message) => {
                      let date = message.date;
                      message.date = new Date(date);
                  });
              }
          }, 60000);
      }


  }


    // Check if currentPage is active, then update user's messagesRead.
    setMessagesRead(userid) {
        this.db.collection('users').doc(userid).collection('conversations').doc(this.job.customerId)
            .get().then('value', snap =>{
            console.log(snap.val());
            if(snap.val() != null){
                this.db.collection('users').doc(userid).collection('conversations').doc(this.job.customerId).update({
                    //messagesRead: snap.val().length
                });
            }
        });
    }

    // Scroll to bottom of page after a short delay.
    scrollBottom(userid) {

        var that = this;
        setTimeout(function() {
            that.content.scrollToBottom();
        }, 300);
        this.setMessagesRead(userid);
    }

    // Scroll to top of the page after a short delay.
    scrollTop() {
        var that = this;
        setTimeout(function() {
            that.content.scrollToTop();
        }, 300);
    }


    // Check if the user is the sender of the message.
    isSender(message) {
        if (message.sender == this.curUser.userid) {
            return true;
        } else {
            return false;
        }
    }


    send(type){
        if (this.message) {
            // User entered a text on messagebox
            if (this.conversationId) {
                let messages = JSON.parse(JSON.stringify(this.messages));
                messages.push({
                    date: new Date().toString(),
                    sender: this.curUser.userid,
                    type: type,
                    message: this.message
                });

                // Update conversation on database.
                this.db.collection('conversations').doc(this.conversationId).update({
                    messages: messages
                });
                // Clear messagebox.
                this.message = '';
                this.scrollBottom(this.curUser.userid);
            } else {
                console.log("else");

                // New Conversation with friend.
                var messages = [];
                messages.push({
                    date: new Date().toString(),
                    sender: this.curUser.userid,
                    type: type,
                    message: this.message
                });
                var users = [];
                users.push(this.curUser.userid);
                users.push(this.job.customerId);
                // Add conversation.
                this.db.collection('conversations').add({
                    dateCreated: new Date().toString(),
                    messages: messages,
                    users: users
                }).then((success) => {
                    console.log(success.id)
                    let conversationId = success.id;
                    this.message = '';
                    // Add conversation reference to the users.

                    this.db.collection('users').doc(this.curUser.userid).collection('conversations').doc(this.job.customerId).set({
                        conversationId: conversationId,
                        messagesRead: 1
                    }).then((res)=>{

                    });
                    this.db.collection('users').doc(this.job.customerId).collection('conversations').doc(this.curUser.userid ).set({
                        conversationId: conversationId,
                        messagesRead: 0
                    }).then((res)=>{

                    });
                });
                this.scrollBottom(this.curUser.userid);
            }
        }

    }

}
