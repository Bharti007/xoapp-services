import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PastJobsPage } from './past-jobs';

@NgModule({
  declarations: [
    PastJobsPage,
  ],
  imports: [
    IonicPageModule.forChild(PastJobsPage),
  ],
})
export class PastJobsPageModule {}
