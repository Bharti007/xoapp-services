import { Component, NgZone } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import * as firebase from 'firebase';
import 'firebase/firestore';
import {LoadingProvider} from "../../providers/loading";


/**
 * Generated class for the PastJobsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-past-jobs',
  templateUrl: 'past-jobs.html',
})
export class PastJobsPage {
    jobs:any = [];
    private db: any;
    public curUser:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public load :LoadingProvider) {
      this.db = firebase.firestore();
      this.curUser = firebase.auth().currentUser;


  }

  ionViewDidLoad() {

  }

    ionViewWillEnter() {
        this.load.show();

        this.db.collection('users').doc(this.curUser.uid).collection('booking')
            .onSnapshot((querySnapshot) => {
                this.load.hide();
                let arr = [];
                querySnapshot.forEach((doc) => {
                    var obj = JSON.parse(JSON.stringify(doc.data()));
                    if(obj.status === 'Completed' || obj.status === 'Cancelled' || obj.status === 'Ended'){
                        arr.push(obj);
                    }

                });
                if (arr.length > 0) {
                    this.jobs = arr;
                } else {
                    this.jobs = [];
                }
                console.log( this.jobs)
            });


    }

}
