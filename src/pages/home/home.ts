import { Component, NgZone } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';


import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from 'angularfire2/firestore';
import {CrudProvider} from "../../providers/crud";

import * as firebase from 'firebase';
import 'firebase/firestore';
import {LoadingProvider} from "../../providers/loading";
import {ServerService} from "../../providers/server-service";
import {MessagesPage} from "../messages/messages";



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
    jobs:any = [];
    private db: any;
    public curUser:any;
  constructor(
              public navCtrl: NavController,
             // public crud: CrudProvider,
              public serverServ:ServerService,
              public load :LoadingProvider) {
      this.db = firebase.firestore();

      setTimeout(() => {
          this.curUser = firebase.auth().currentUser;
          console.log(this.curUser);
          if (!this.curUser.emailVerified) {
              this.curUser.sendEmailVerification().then(function () {
                  firebase.auth().signOut();
                  var msg = "Email has been sent. Please verify your email first.";
                  // this.presentAlert(msg);
                  alert("Email has been sent. Please verify your email first.");

                  // setTimeout(() => {
                  //     let alert = this.alertCtrl.create({
                  //         title: 'ALERT',
                  //         subTitle: msg,
                  //         buttons: ['Dismiss']
                  //     });
                  //     alert.present();
                  // })


              }, function (error) {
                  console.log(error);
              });
          }

      });

  }

    ionViewDidLoad() {
        //to get the current user
       var user = firebase.auth().currentUser;

        if(user != null){
            this.db
                .collection('users')
                .doc(user.uid)
                .get()
               .then((result)=>{
                   console.log(result.data());

                    if(result != null){
                        this.curUser = result.data();
                        if (!this.curUser.isActive) {
                            firebase.auth().signOut();
                            var msg = "Please Contact XoApp Authority to activate your account";
                            // this.presentAlert(msg);
                            alert(msg);


                        }
                    }
                })
            }

        this.jobs = [];
        this.showJobs();


    }

    ionViewWillEnter() {
        this.jobs = [];
     this.showJobs();

    }

    showJobs(){

        this.db.collection('bookings')
            .onSnapshot((querySnapshot) => {
                let arr = [];
                this.load.show();
                querySnapshot.forEach((doc) => {
                    var obj = JSON.parse(JSON.stringify(doc.data()));
                    console.log(obj);
                    if(obj.status === 'Waiting for reply'){
                        obj.$key = doc.id
                        obj["currentBid"] = obj.estimatedBudget;
                        obj['priceAfterBid'] = obj.estimatedBudget // to get the first price
                        obj["lowerLimit"] = (obj["currentBid"] / 2).toFixed(2);
                        arr.push(obj);
                    }
                    else if(obj.status === 'Bidding started'){

                        if (obj.hasOwnProperty('bidding')) {
                            for (let i = 0; i < obj.bidding.length; i++) {
                                if (obj.bidding[i].serviceProvId == this.curUser.userid) {
                                    obj["isBidding"] = true; //current user already bid for this ride
                                    obj["yourBid"] = obj.bidding[i].bid;
                                }
                                if (obj.bidding.length > 1) {
                                    let bidLength = obj.bidding.length
                                    let amt = obj.bidding[bidLength - 1].bid;
                                    // obj["changeBidcolor"] = true
                                    obj["currentBid"] = parseFloat(amt);
                                }
                                else if(obj.bidding.length == 1){
                                    obj["currentBid"] = parseFloat(obj.bidding[0].bid);
                                }
                                else {
                                    // obj["changeBidcolor"] = false;
                                    obj["currentBid"] = parseFloat(obj.estimatedBudget);
                                }
                            }
                           // obj['actualEstimatedBudget'] = obj.estimatedBudget
                        }
                        obj["lowerLimit"] = (obj["currentBid"] / 2).toFixed(2);

                        arr.push(obj);

                    }
                });
                console.log(arr)
                if (arr.length > 0) {
                    this.jobs = arr;
                } else {
                    this.jobs = [];
                }
                 console.log(this.jobs);
                this.load.hide();
            });

    }

    changeBid(action, job) {
        //this.changeBidCalled=true;
        delete job.yourBid;

        if (action == 'inc') {
            job.priceAfterBid += 1;
        }
        else if (action == 'dec' && job.priceAfterBid > job.lowerLimit) {
            job.priceAfterBid -= 1;
        }

    }

    yourBid(job){
      var bookingTable = this.db.collection('bookings');
      var customerBooking = this.db.collection('users').doc(job.customerId).collection('booking');
      let bookingId = job.$key;
       var bidding=[];
       var data = {};
       data['serviceProvId'] = this.curUser.userid;
        data['serviceProvusername'] = this.curUser.username;
        data['serviceProvemail'] = this.curUser.email;
        data['serviceProvimage'] = this.curUser.image;
        if(this.curUser.hasOwnProperty('pushToken')){
            data['serviceProvpushToken'] = this.curUser.pushToken;
        }
        data['bid'] = job.priceAfterBid;
        data['jobId'] = bookingId;
        data['status']='Bidding started'
        bookingTable.doc(bookingId).get().then((bookingServer)=>{
            let bookingData = bookingServer.data();
          if(bookingData.hasOwnProperty('bidding') && bookingData.bidding.length > 0){
              for (var j = 0; j < bookingData.bidding.length; j++) {
                  if (bookingData.bidding[j].serviceProvId !== this.curUser.userid) {
                      bidding.push(bookingData.bidding[j]);
                  }
                  else if(bookingData.bidding[j].serviceProvId === this.curUser.userid){
                      bidding.pop();
                     // bidding.push(data);
                   }
              }
              bidding.push(data)
          }
          else{
              bidding=[data];
          }
            bookingTable.doc(bookingId).update({'bidding':bidding,'status':"Bidding started",'$key':job.$key,"priceAfterBid":job.priceAfterBid,'serviceProvimage':this.curUser.image}).then(()=>{

            });
            customerBooking.doc(bookingId).update({'bidding':bidding,'status':"Bidding started","priceAfterBid":job.priceAfterBid,'serviceProvimage':this.curUser.image});

           // this.serverServ.sendPush('Bid by '+this.curUser.username,  this.curUser.username + " bid your booking.", job.customerPushtoken).subscribe(response => console.log(response));
      });

    }

    negotiateJob(job){
        this.navCtrl.push(MessagesPage,{"job":job});
    }

}
