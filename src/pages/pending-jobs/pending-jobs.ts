import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as firebase from 'firebase';
import 'firebase/firestore';
import {LoadingProvider} from "../../providers/loading";
import {CurrentJobsPage} from "../current-jobs/current-jobs";
/**
 * Generated class for the PendingJobsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-pending-jobs',
  templateUrl: 'pending-jobs.html',
})
export class PendingJobsPage {
    jobs:any = [];
    private db: any;
    public curUser:any;
    constructor(public navCtrl: NavController, public navParams: NavParams,public load :LoadingProvider) {
        this.db = firebase.firestore();
        this.curUser = firebase.auth().currentUser;
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad BookingPage');
    }

    ionViewWillEnter() {
        this.load.show();
        this.db.collection('users').doc(this.curUser.uid).collection('booking')
            .onSnapshot((querySnapshot) => {
                let arr = [];
                querySnapshot.forEach((doc) => {
                    var obj = JSON.parse(JSON.stringify(doc.data()));
                    if(obj.status !== 'Completed' && obj.status !== 'Cancelled' && obj.status !== 'Review' && obj.status !== 'Cancelled'){
                        arr.push(obj);
                    }

                });
                if (arr.length > 0) {
                    this.jobs = arr;
                } else {
                    this.jobs = [];
                }
                console.log( this.jobs);
                this.load.hide();
            })


    };

    acceptJOb(job){
        this.navCtrl.push(CurrentJobsPage,{'job':job});
    };

}
