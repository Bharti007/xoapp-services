import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';
import * as firebase from 'firebase';
import { Settings } from '../settings';




@Injectable()
export class ServerService {
  public fireAuth: any;
  constructor(public http: Http) {
    this.fireAuth = firebase.auth();

  }



  //password reset
  passReset(email) {
    return this.fireAuth.sendPasswordResetEmail(email).then((success) => {
      console.log(success);
    })
      .catch((error) => {
        console.log(error);
      });
  }

  addStripeCard(card, stripeId, email): Observable<string> {
    let body = {
      "number": card.cardNumber,
      "exp_month": card.expiryMonth,
      "exp_year": card.expiryYear,
      "cvc": card.cvv,
      "email": email,
      "stripeId": stripeId
    }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post("https://us-central1-xoapp-6fc68.cloudfunctions.net/addStripeCard", body, options)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  stripePayment(card, cust_id, amount): Observable<string> {
    console.log("card..................", card);
    let body = {
      "customer_id": cust_id,
      "card_id": card,
      "amount": amount
    }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post("https://us-central1-xoapp-6fc68.cloudfunctions.net/stripPayment", body, options)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  deleteStripeCard(card, cust_id): Observable<string> {
    console.log("card..................", card);
    let body = {
      "customer_id": cust_id,
      "card_id": card
    }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post("https://us-central1-xoapp-6fc68.cloudfunctions.net/deleteStripeCard", body, options)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  sendPush(title, msg, token): Observable<string> {
    console.log(token);
    let body = {
      "to": token,
      "notification": {
        "body": msg,
        "title": title
      }
    }
    let headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': Settings.fcm_api_key
    });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(Settings.fcm_api_url, body, options)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

}
