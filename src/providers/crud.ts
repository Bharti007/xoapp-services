import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import 'firebase/firestore';
import {LogoutProvider} from "./logout";
@Injectable()
export class CrudProvider {
    public db: any;
  constructor(public logout:LogoutProvider) {
    this.db = firebase.firestore();
    console.log("Initializing Loading Provider");
  }


  //getCurrentUser
    getCurrentUser(): Promise<any> {
        return new Promise((resolve, reject) => {
            let curUser = firebase.auth().currentUser;
            if(curUser){
                resolve(curUser);
            }
            else{
                this.logout.logout();
             //   reject(null);
            }
        });
    }

    //get all the documents from the collection
    getAllDocuments(collection: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.db.collection(collection)
                .get()
                .then((querySnapshot) => {
                    let arr = [];
                    querySnapshot.forEach(function (doc) {
                        var obj = JSON.parse(JSON.stringify(doc.data()));
                        obj.$key = doc.id
                        console.log(obj)
                        arr.push(obj);
                    });

                    if (arr.length > 0) {
                        console.log("Document data:", arr);
                        resolve(arr);
                    } else {
                        console.log("No such document!");
                        resolve(null);
                    }


                })
                .catch((error: any) => {
                    reject(error);
                });
        });
    }

    //delete document to the collection
    deleteDocument(collectionName: string, docID: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.db
                .collection(collectionName)
                .doc(docID)
                .delete()
                .then((obj: any) => {
                    resolve(obj);
                })
                .catch((error: any) => {
                    reject(error);
                });
        });
    }

    //add document to the collection(Will create collection if does not exits,will create new document)
    addDocument(collectionName: string, dataObj: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.db.collection(collectionName).add(dataObj)
                .then((obj: any) => {
                    resolve(obj);
                })
                .catch((error: any) => {
                    reject(error);
                });
        });
    }



    //insert document to the collection(Will create collection if does not exits)
    insertDocument(collectionName: string, dataObj: any,docID: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.db.collection(collectionName) .doc(docID).set(dataObj)
                .then((obj: any) => {
                    console.log(obj);
                    resolve(obj);
                })
                .catch((error: any) => {
                    reject(error);
                });
        });
    }

    //update document to the collection
    updateDocument(collectionName: string, docID: string, dataObj: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.db
                .collection(collectionName)
                .doc(docID)
                .update(dataObj)
                .then((obj: any) => {
                    resolve(obj);
                })
                .catch((error: any) => {
                    reject(error);
                });
        });
    }

    //update document to the collection
    getSingleDocument(collectionName: string, docID: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.db
                .collection(collectionName)
                .doc(docID)
                .get()
                .then((obj: any) => {
                    if (!obj.exists) {
                        console.log('No such document!');
                        reject(null);
                    } else {
                        resolve(obj.data());
                    }


                })
                .catch((error: any) => {
                    reject(error);
                });
        });
    }

}
