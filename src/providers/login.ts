import { Injectable, NgZone } from '@angular/core';
import {  Platform, ToastController, AlertController } from 'ionic-angular';
import { LoadingProvider } from './loading';
import {LogoutProvider} from "./logout";
import { AlertProvider } from './alert';
import { CrudProvider } from './crud';
import * as firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import {  AngularFirestore} from 'angularfire2/firestore';
import 'firebase/firestore';
import {App} from "ionic-angular";
import {Firebase} from "@ionic-native/firebase";


@Injectable()
export class LoginProvider {
    private firestoreDB = firebase.firestore();
  constructor(public app: App,public afstore: AngularFirestore,public loadingProvider: LoadingProvider, public alertProvider: AlertProvider, public zone: NgZone,
    public platform: Platform, public afAuth: AngularFireAuth,  public toastCtrl: ToastController, public crud:CrudProvider,public logout:LogoutProvider,private cordovaFirebase: Firebase) {
      firebase.auth().onAuthStateChanged((user) => {
          if (user) {
              if (user["isAnonymous"]) {
                  // nav.setRoot(Settings.Loginpage, { animate: false });
              } else {
                  this.zone.run(() => {
                      this.cordovaFirebase.grantPermission();
                      this.cordovaFirebase.getToken()
                          .then(token => {
                              console.log(token)
                              this.firestoreDB.collection('users').doc(user.uid).update({'pushToken':token}).then((data)=>{
                                  //console.log(data);
                              });
                              //firebase.database().ref('users/' + user.uid + '/pushToken').set(token);
                          })
                          .catch(error => {

                          });
                      //nav.setRoot(Settings.tabsPage,{ animate: false });
                  });
              }
          }
      });
  }

   // Login on Firebase given the email and password.
  emailLogin(email, password) {
    this.loadingProvider.show();
    firebase.auth().signInWithEmailAndPassword(email, password)
      .then((success) => {
        this.loadingProvider.hide();
       //  this.navCtrl.setRoot(Settings.homePage, { animate: false });
      })
      .catch((error) => {
        this.loadingProvider.hide();
        let code = error["code"];
        this.alertProvider.showErrorMessage(code);
      });
  }

  // Register user on Firebase given the email and password.
  register(registerData) {
    this.loadingProvider.show();
    var password = registerData.password;
    firebase.auth().createUserWithEmailAndPassword(registerData.email, password)
      .then((success) => {

          let user=firebase.auth().currentUser;
         // firebase.auth().currentUser.sendEmailVerification();
          delete registerData.password;
          delete registerData.confirmpassword;
          registerData.userid = user.uid;
          registerData.image= "assets/imgs/profile.png";
          registerData.createdOn =  new Date();
          registerData.online=false;
          registerData.isActive=false;
          registerData.provider='Email';
          registerData.type='serviceprovider';
          registerData['pushToken']='';

          this.crud.insertDocument('users',registerData,user.uid).then((resp)=>{
              this.loadingProvider.hide();
          });
          success.sendEmailVerification().then(function () {
              this.emailLogin(registerData.email, password);
              console.log("please check email");
          }, function (error) {
              // An error happened.
          });

      })
      .catch((error) => {
        this.loadingProvider.hide();
        let code = error["code"];
        this.alertProvider.showErrorMessage(code);
      });
  }

  // Send Password Reset Email to the user.
    // Send Password Reset Email to the user.
    sendPasswordReset(email) {
        return firebase.auth().sendPasswordResetEmail(email).then((success) => {
            console.log(success);
        })
            .catch((error) => {
                console.log(error);
            });
    }



}
