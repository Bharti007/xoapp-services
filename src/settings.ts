import { TabsPage } from './pages/tabs/tabs';
import { HomePage } from './pages/home/home';
import { LoginPage } from  './pages/login/login';

export namespace Settings {

    export const firebaseConfig = {
        apiKey: "AIzaSyCDaojudyAOTaYoGWfisNv8xPUekNFKwh0",
        authDomain: "xoapp-6fc68.firebaseapp.com",
        databaseURL: "https://xoapp-6fc68.firebaseio.com",
        projectId: "xoapp-6fc68",
        storageBucket: "xoapp-6fc68.appspot.com",
        messagingSenderId: "317204791673"
    };

    export const facebookLoginEnabled = true;
    export const googleLoginEnabled = true;

    export const facebookAppId: string = "614038478991749";
    export const googleClientId: string = "317204791673-jjkteq51jdcnv3bmtfglmn63d9dpo7k8.apps.googleusercontent.com";
    // export const customTokenUrl: string = "https://us-central1-chat-module-990a9.cloudfunctions.net/getCustomToken";
    export const fcm_api_key :string = "key=AAAASdrhAXk:APA91bG6ErEsR6LlyTZqVG_Y5ZgKq0pS1y9SOnxyhk9kKCAjmA2LuwAdoDfQZXM2sXhAazvC-sHEYPa2X5FuOU0c48T4zFjlYLJ7LyNYvhjj3-XKDyREIegsG8PXUhl7Y66t6DDvsRLqgBCuM7oklt08HrsOv7Op_Q";
    export const fcm_api_url:string="https://fcm.googleapis.com/fcm/send"  ;
    export const homePage = HomePage;
    export const tabsPage = TabsPage;
    export const loginPage = LoginPage;


}