import { Component,NgZone ,ViewChild} from '@angular/core';
import {Nav, Platform,ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Firebase } from '@ionic-native/firebase';
//firebase
import * as firebase from 'firebase';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import {LogoutProvider} from "../providers/logout";
import {ProfilePage} from "../pages/profile/profile";


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  //rootPage:any = TabsPage;
    @ViewChild(Nav) nav: Nav;
    rootPage:any=LoginPage;
    public counter:any=0;
    pages: Array<{title: string, component: any, icon: any}>;
  constructor(public toastCtrl: ToastController,public zone:NgZone,private cordovaFirebase: Firebase,public platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,public logouProv:LogoutProvider) {
      this.pages = [
          { title: 'Home', component: TabsPage, icon: 'md-home'},
          // { title: 'Current Bookings', component: BookingPage, icon: 'md-bookmarks' },
          // { title: 'Booking History', component: PastbookingPage, icon: 'md-bookmarks' },
          { title: 'Profile', component: ProfilePage, icon: 'md-people' },
          // { title: 'Payments', component: PaymentsPage, icon: 'md-card' },
          { title: 'Logout', component: null, icon: 'md-exit' }
      ];

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
        platform.registerBackButtonAction(() => {
            if (this.counter == 0) {
                this.counter++;
                this.presentToast();
                setTimeout(() => { this.counter = 0 }, 3000)
            } else {
                // console.log("exitapp");
                platform.exitApp();
            }
        }, 0);
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                this.cordovaFirebase.grantPermission();
                this.cordovaFirebase.getToken()
                    .then(token => {
                        var db =firebase.firestore();
                        db.collection('users').doc(user.uid).update({'pushToken':token});
                        //firebase.database().ref('users/' + user.uid + '/pushToken').set(token);
                    })
                    .catch(error => {

                    });

                this.zone.run(()=>{
                    this.rootPage = TabsPage;
                });
            } else {
                this.zone.run(()=>{
                    this.rootPage = LoginPage;
                });

            }
        });
    });
  }


    presentToast() {
        let toast = this.toastCtrl.create({
            message: "Press again to exit",
            duration: 3000,
            position: "bottom"
        });
        toast.present();
    }

    changePage(page) {
        if(page.component==null){
            this.logouProv.logout();
        }else{
            this.nav.push(page.component);
        }

    }
}
