import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import {RegisterPage} from "../pages/register/register";
import {LoginPage} from "../pages/login/login";
import {ProfilePage} from "../pages/profile/profile";
import {PastJobsPage} from "../pages/past-jobs/past-jobs";
import {PendingJobsPage} from "../pages/pending-jobs/pending-jobs";

import { Firebase } from '@ionic-native/firebase';

import * as firebase from 'firebase';
import 'firebase/firestore';

import { HttpModule } from '@angular/http';
import { Settings } from '../settings';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import {AngularFirestore} from 'angularfire2/firestore';

import {LoginProvider} from "../providers/login";
import {LogoutProvider} from "../providers/logout";
import {LoadingProvider} from "../providers/loading";
import {CrudProvider} from "../providers/crud";
import {AlertProvider} from "../providers/alert";



import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {CallNumber} from '@ionic-native/call-number';
import {ServerService} from "../providers/server-service";
import {CurrentJobsPage} from "../pages/current-jobs/current-jobs";

import {MessagesPage} from "../pages/messages/messages";

import { DateFormatPipe } from '../pipes/date';
import { Camera } from '@ionic-native/camera';
firebase.initializeApp(Settings.firebaseConfig);

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
      RegisterPage,
      LoginPage,
      ProfilePage,
      PastJobsPage,
      PendingJobsPage,
      CurrentJobsPage,
      MessagesPage,
      DateFormatPipe
  ],
  imports: [
      BrowserModule,
      IonicModule.forRoot(MyApp,{tabsHideOnSubPages: true, scrollAssist: false}),
      AngularFireModule.initializeApp(Settings.firebaseConfig,'xolo'),
      AngularFireAuthModule,
      AngularFireDatabaseModule,
      HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
      RegisterPage,
      LoginPage,
      ProfilePage,
      PastJobsPage,
      PendingJobsPage,
      CurrentJobsPage,
      MessagesPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
      Firebase,
      LoginProvider,
      LogoutProvider,
      LoadingProvider,
      CrudProvider,
      AlertProvider,
      ServerService,
      CallNumber,
      Camera,
      {provide: ErrorHandler, useClass: IonicErrorHandler},
       AngularFirestore
  ]
})
export class AppModule {}
